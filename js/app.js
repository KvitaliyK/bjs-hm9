// Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// DOM - це об'єктна модель документа, в якій можна взаємодіяти з елементами веб-сайту

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// Властивість innerHTML повертає: текстовий вміст елемента, включаючи всі пробіли та внутрішні теги HTML. 
// Властивість innerText повертає: лише текстовий вміст елемента та всіх його дочірніх елементів

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// getElementById, getElementsByClassName, getElementsByTagName, querySelector, querySelectorAll
// Якщо потрібно звернутись за id, то потрібно використати getElementById.
// Якщо потрібно звертатися до елементів за класом, тегом або іншим селектором, то потрібно використати querySelector або querySelectorAll

// 4. Яка різниця між nodeList та HTMLCollection?
// Основна відмінність полягає в тому, що NodeList може містити будь-який тип вузла, тоді як HTMLCollection містить лише елементи



// Практичні завдання
// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
// Використайте 2 способи для пошуку елементів.
// Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
const featureQuery = document.querySelectorAll(".feature")
featureQuery.forEach(element => {
    element.style.textAlign = 'center';
    console.log(element);
});

const featureClass = document.getElementsByClassName("feature")
Array.from(featureClass).forEach(element => {
    element.style.textAlign = 'center';
    console.log(element.outerHTML);
});

// 2. Змініть текст усіх елементів h2 на "Awesome feature".
const h2Elements = document.querySelectorAll('h2')
h2Elements.forEach(el => {
    el.textContent = 'Awesome feature'
})

// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
const featureTitle = document.querySelectorAll(".feature-title")
h2Elements.forEach(el => {
    el.textContent += '!'
})

